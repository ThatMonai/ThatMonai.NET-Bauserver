package net.thatmonai.bauserver.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;
import net.thatmonai.bauserver.utils.ConfigManager;

public class delportCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player)sender;
		
		if(sender instanceof Player) {
		
			if(cmd.getName().equalsIgnoreCase("delport")) {
				
				if(player.hasPermission("delport")) {
					
					if(args.length == 1) {
						
						if(ConfigManager.existsWarp(args[0]) == false) {
							
							ConfigManager.delWarp(args[0]);
							player.sendMessage(Bauserver.prefix + "Der Port �c" + args[0] + " �7wurde gel�scht!");
						
						} else player.sendMessage(Bauserver.prefix + "Der Port �c" + args[0] + " �7existiert nicht!"); 
						
					} else player.sendMessage(Bauserver.syntax + "/delport <Portname>");
					
				} else player.sendMessage(Bauserver.noperms);
				
			} else player.sendMessage(Bauserver.syntax + "/delport <Portname>");
			
		} else Bukkit.getConsoleSender().sendMessage(Bauserver.noperms);
		
		return false;
	}

}
