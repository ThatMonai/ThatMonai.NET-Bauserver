package net.thatmonai.bauserver.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;
import net.thatmonai.bauserver.utils.ConfigManager;

public class portlistCMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player)sender;
		
		if(sender instanceof Player){
			
			if(cmd.getName().equalsIgnoreCase("portlist")) {
				
				if(player.hasPermission("portlist")) {
					
					if(args.length == 0) {

						ConfigManager.listWarp(player);

					} else player.sendMessage(Bauserver.syntax + "/portlist");
					
				} else player.sendMessage(Bauserver.noperms);
				
			} else player.sendMessage(Bauserver.syntax + "/portlist");
			
		} else Bukkit.getConsoleSender().sendMessage(Bauserver.noplayer);
		
		return false;
	}

}
