package net.thatmonai.bauserver.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;
import net.thatmonai.bauserver.utils.ItemAPI;

public class buildCMD implements CommandExecutor {
	
	public static ArrayList<Player> build = new ArrayList<>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player)sender;

		if(sender instanceof Player) {
		
			if(cmd.getName().equalsIgnoreCase("build")) {
			
				if(player.hasPermission("build")) {
					
					if(args.length == 0) {
						
						if(!build.contains(player)) {
							
							player.setGameMode(GameMode.CREATIVE);
							player.sendMessage(Bauserver.prefix + "�aDu kannst nun bauen!");
							player.getInventory().setItem(0, ItemAPI.createItem(Material.WOOD_AXE, 0, "�cHolzaxt"));
							player.getInventory().setItem(1, ItemAPI.createItem(Material.ARROW, 0, "�cPfeil"));
							player.getInventory().setItem(2, ItemAPI.createItem(Material.BLAZE_ROD, 0, "�cLohenrute"));
							build.add(player);
							
						} else if(build.contains(player)) {
							
							player.setGameMode(GameMode.CREATIVE);
							player.sendMessage(Bauserver.prefix + "�cDu kannst nun nicht mehr bauen!");
							player.getInventory().clear();
							build.remove(player);
							
						} else player.sendMessage(Bauserver.prefix + "Es ist ein Fehler aufgetreten!");
						
					} else if(args.length == 1) {
						
						Player target = Bukkit.getPlayer(args[0]);
						
						if(!build.contains(target)) {
							
							target.setGameMode(GameMode.CREATIVE);
							target.sendMessage(Bauserver.prefix + "�aDu kannst nun bauen!");
							target.getInventory().setItem(0, ItemAPI.createItem(Material.WOOD_AXE, 0, "�cHolzaxt"));
							target.getInventory().setItem(1, ItemAPI.createItem(Material.ARROW, 0, "�cPfeil"));
							target.getInventory().setItem(2, ItemAPI.createItem(Material.BLAZE_ROD, 0, "�cLohenrute"));
							build.add(target);
							
							player.sendMessage(Bauserver.prefix + target.getName() + " �akann nun bauen!");
							
						} else if(build.contains(target)) {
						
							target.setGameMode(GameMode.CREATIVE);
							target.sendMessage(Bauserver.prefix + "�cDu kannst nun nicht mehr bauen!");
							target.getInventory().clear();
							build.remove(target);
							
							player.sendMessage(Bauserver.prefix + target.getName() + " �ckann nun nicht mehr bauen!");
							
						} else player.sendMessage(Bauserver.prefix + "Es ist ein Fehler aufgetreten!");
						
					} else player.sendMessage(Bauserver.syntax + "/build <Spieler>");
					
				} else player.sendMessage(Bauserver.noperms);
			
			} else player.sendMessage(Bauserver.syntax + "/build <Spieler>");
			
		} else Bukkit.getConsoleSender().sendMessage(Bauserver.noplayer);
		
		return false;
	}

}
