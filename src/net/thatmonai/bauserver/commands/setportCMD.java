package net.thatmonai.bauserver.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;
import net.thatmonai.bauserver.utils.ConfigManager;

public class setportCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player)sender;
		
		if(sender instanceof Player) {
			
			if(cmd.getName().equalsIgnoreCase("setport")) {
				
				if(player.hasPermission("setport")) {
					
					if(args.length == 1) {
						
						ConfigManager.setWarpA(args[0], player);
						
						if(ConfigManager.existsWarp(args[0]) == true) {
							
							ConfigManager.setWarp(args[0], player);
							player.sendMessage(Bauserver.prefix + "Der Port �c" + args[0] + " �7wurde gesetzt!");
							
						} else player.sendMessage(Bauserver.prefix + "Der Port �c" + args[0] + " �7existiert bereits!");
						
					} else player.sendMessage(Bauserver.syntax + "/setport <Portname>");
					
				} else player.sendMessage(Bauserver.noperms);
				
			} else player.sendMessage(Bauserver.syntax + "/setport <Portname>");
			
		} else Bukkit.getConsoleSender().sendMessage(Bauserver.noplayer);
		
		return false;
	}

}
