package net.thatmonai.bauserver.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;

public class speedCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	
		Player player = (Player)sender;
		
		if(sender instanceof Player) {
		
			if(cmd.getName().equalsIgnoreCase("speed")) {
			
				if(player.hasPermission("speed")) {
					
					if(args.length == 1) {
						
						if(args[0].equals("1")) {
							
							player.setFlySpeed(0.1F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("2")) {
							
							player.setFlySpeed(0.2F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("3")) {
							
							player.setFlySpeed(0.3F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("4")) {
							
							player.setFlySpeed(0.4F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("5")) {
							
							player.setFlySpeed(0.5F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("6")) {
							
							player.setFlySpeed(0.6F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("7")) {
							
							player.setFlySpeed(0.7F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("8")) {
							
							player.setFlySpeed(0.8F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("9")) {
							
							player.setFlySpeed(0.9F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else if(args[0].equals("10")) {
							
							player.setFlySpeed(1F);
							player.sendMessage(Bauserver.prefix + "Dein Speed wurde auf �c" + args[0] +" �7gesetzt!");
							
						} else player.sendMessage(Bauserver.syntax + "/speed <Geschwindigkeit 1-10>");
							
					} else player.sendMessage(Bauserver.syntax + "/speed <Geschwindigkeit 1-10>");
					
				} else player.sendMessage(Bauserver.noperms);
			
			} else player.sendMessage(Bauserver.syntax + "/speed <Geschwindigkeit 1-10>");
			
		} else Bukkit.getConsoleSender().sendMessage(Bauserver.noplayer);
		
		return false;
	}
}