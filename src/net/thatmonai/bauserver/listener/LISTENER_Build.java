package net.thatmonai.bauserver.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import net.thatmonai.bauserver.commands.buildCMD;

public class LISTENER_Build implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		
		if(!buildCMD.build.contains(event.getPlayer())) {
			
			event.setCancelled(true);
			
		}
		
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		
		if(!buildCMD.build.contains(event.getPlayer())) {
			
			event.setCancelled(true);
			
		}
		
	}
	
}
