package net.thatmonai.bauserver;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.thatmonai.bauserver.commands.buildCMD;
import net.thatmonai.bauserver.commands.delportCMD;
import net.thatmonai.bauserver.commands.portCMD;
import net.thatmonai.bauserver.commands.portlistCMD;
import net.thatmonai.bauserver.commands.setportCMD;
import net.thatmonai.bauserver.commands.speedCMD;
import net.thatmonai.bauserver.listener.LISTENER_Build;
import net.thatmonai.bauserver.listener.LISTENER_Damage;
import net.thatmonai.bauserver.listener.LISTENER_Drop;
import net.thatmonai.bauserver.listener.LISTENER_Feed;
import net.thatmonai.bauserver.listener.LISTENER_Join;
import net.thatmonai.bauserver.listener.LISTENER_Quit;

public class Bauserver extends JavaPlugin {
	
	public static String prefix = "�8[�6Bauserver�8] �7";
	public static String noperms = "�cDazu hast du keine Rechte!";
	public static String noplayer = "�cDies kann nur ein Spieler tun!";
	public static String syntax = "�cSyntax:�c ";
	
	public static Bauserver plugin;
	
	@Override
	public void onEnable() {
	
		plugin = this;
		
		getCommand("build").setExecutor(new buildCMD());
		getCommand("speed").setExecutor(new speedCMD());
		getCommand("port").setExecutor(new portCMD());
		getCommand("setport").setExecutor(new setportCMD());
		getCommand("delport").setExecutor(new delportCMD());
		getCommand("portlist").setExecutor(new portlistCMD());
		
		Bukkit.getPluginManager().registerEvents(new LISTENER_Join(), this);
		Bukkit.getPluginManager().registerEvents(new LISTENER_Quit(), this);
		Bukkit.getPluginManager().registerEvents(new LISTENER_Feed(), this);
		Bukkit.getPluginManager().registerEvents(new LISTENER_Build(), this);
		Bukkit.getPluginManager().registerEvents(new LISTENER_Damage(), this);
		Bukkit.getPluginManager().registerEvents(new LISTENER_Drop(), this);

	}

	@Override
	public void onDisable() {
		
		
		
	}
	
	public static Bauserver plugin() {
		
		return plugin;
	}
	
}
