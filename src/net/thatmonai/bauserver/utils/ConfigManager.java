package net.thatmonai.bauserver.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.thatmonai.bauserver.Bauserver;

public class ConfigManager {

public static void createSpawn() {
		
		File ordner = new File("plugins/lobby");
		File file = new File("plugins/lobby/Spawn.yml");
		
		if(!ordner.exists()) {
			ordner.mkdirs();
		}
		if(!file.exists()) {		
			try {
				file.createNewFile();
				
				YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
				
				cfg.set("Spawn.X", "X");
				cfg.set("Spawn.Y", "Y");
				cfg.set("Spawn.Z", "Z");
				cfg.set("Spawn.World", "null");
				cfg.set("Spawn.Yaw", "null");
				cfg.set("Spawn.Pitch", "null");
				
				cfg.set("Skywars.X", "X");
				cfg.set("Skywars.Y", "Y");
				cfg.set("Skywars.Z", "Z");
				cfg.set("Skywars.World", "null");
				cfg.set("Skywars.Yaw", "null");
				cfg.set("Skywars.Pitch", "null");
				
				try {
					cfg.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
		
	
	}
	
	
	public static boolean getSpawn() {
		
		File file = new File("plugins/lobby/Spawn.yml");
		
		if(file.exists()) {
			
			return true;
			
		} else {
			
			return false;
		}
		
	}
	
	
	
	public static void setSpawn(Player p) {
	
		File file = new File("plugins/lobby/Spawn.yml");
		Location loc = p.getLocation();
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		cfg.set("Spawn.X", loc.getX());
		cfg.set("Spawn.Y", loc.getY());
		cfg.set("Spawn.Z", loc.getZ());
		cfg.set("Spawn.World", loc.getWorld().getName());
		cfg.set("Spawn.Yaw", loc.getYaw());
		cfg.set("Spawn.Pitch", loc.getPitch());
		
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}	
		
	public static Location getSpawn(Player p) {

		File file = new File("plugins/lobby/Spawn.yml");
		Location loc = p.getLocation();
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		loc.setX(cfg.getDouble("Spawn.X"));
		loc.setY(cfg.getDouble("Spawn.Y") + 1);
		loc.setZ(cfg.getDouble("Spawn.Z"));
		loc.setYaw((float)cfg.getDouble("Spawn.Yaw"));
		loc.setPitch((float)cfg.getDouble("Spawn.Pitch"));
		loc.setWorld(Bukkit.getWorld(cfg.getString("Spawn.World")));

		return loc;
	}
	


//_____________________________________________________________

	public static void setWarpA(String name, Player player) {
	
		File ordner = new File("plugins/bauserver");
		File file = new File("plugins/bauserver/Warps.yml");
	
		if(!ordner.exists()) {
			ordner.mkdirs();
		}
		if(!file.exists()) {		
			try {
				file.createNewFile();
			
				YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			
				Location loc = player.getLocation();
				
				cfg.set("Warps." + name + ".X", loc.getX());
				cfg.set("Warps." + name + ".Y", loc.getY());
				cfg.set("Warps." + name + ".Z", loc.getZ());
				cfg.set("Warps." + name + ".Yaw", loc.getYaw());
				cfg.set("Warps." + name + ".Pitch", loc.getPitch());
				cfg.set("Warps." + name + ".World", loc.getWorld().getName());
			
				try {
					cfg.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	
	}
	
	public static void setWarp(String name, Player player) {
		
		File file = new File("plugins/bauserver/Warps.yml");
		Location loc = player.getLocation();
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		cfg.set("Warps." + name + ".X", loc.getX());
		cfg.set("Warps." + name + ".Y", loc.getY());
		cfg.set("Warps." + name + ".Z", loc.getZ());
		cfg.set("Warps." + name + ".Yaw", loc.getYaw());
		cfg.set("Warps." + name + ".Pitch", loc.getPitch());
		cfg.set("Warps." + name + ".World", loc.getWorld().getName());
		
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static Location getWarp(String name, Player player) {

		File file = new File("plugins/bauserver/Warps.yml");
		Location loc = player.getLocation();
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		loc.setX(cfg.getDouble("Warps." + name + ".X"));
		loc.setY(cfg.getDouble("Warps." + name + ".Y") + 1);
		loc.setZ(cfg.getDouble("Warps." + name + ".Z"));
		loc.setYaw((float)cfg.getDouble("Warps." + name + ".Yaw"));
		loc.setPitch((float)cfg.getDouble("Warps." + name + ".Pitch"));
		loc.setWorld(Bukkit.getWorld(cfg.getString("Warps." + name + ".World")));

		return loc;
	}

	public static void delWarp(String name) {
		
		File file = new File("plugins/bauserver/Warps.yml");
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		cfg.set("Warps." + name, null);
		cfg.set("Warps." + name + ".X", null);
		cfg.set("Warps." + name + ".Y", null);
		cfg.set("Warps." + name + ".Z", null);
		cfg.set("Warps." + name + ".Yaw", null);
		cfg.set("Warps." + name + ".Pitch", null);
		cfg.set("Warps." + name + ".World", null);

		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static boolean existsWarp(String name) {
		
		File file = new File("plugins/bauserver/Warps.yml");
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		if(cfg.get("Warps." + name + ".World") == null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static boolean existsWarpList() {
		
		File file = new File("plugins/bauserver/Warps.yml");
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		if(cfg.get("Warps.") == null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static void listWarp(Player player) {
		
		File file = new File("plugins/bauserver/Warps.yml");
		
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		String str = ""+ Bauserver.prefix + "Folgende Ports sind verf�gbar: �c";
		for(String warp : cfg.getConfigurationSection("Warps").getKeys(false)) {
			str += warp + "�7, �c";
		}

		player.sendMessage(str);
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}

