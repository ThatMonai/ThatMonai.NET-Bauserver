package net.thatmonai.bauserver.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemAPI {

public static ItemStack createItem(Material material, int subid, String displayname) {
		
		ItemStack Item = new ItemStack(material, 1, (short) subid);
		ItemMeta mitem = Item.getItemMeta();	
		mitem.setDisplayName(displayname);
		Item.setItemMeta(mitem);

		return Item;
	}	
	
}
